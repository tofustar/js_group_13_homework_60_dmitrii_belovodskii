import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { RouletteService } from '../shared/roulette.service';

@Directive({
  selector: '[appColor]'
})

export class ColorDirective implements OnInit {


  @Input() set appColor(number: string) {
    this.colorClass = this.rouletteService.getColor(parseInt(number));
  };

  colorClass = '';

  constructor(private el: ElementRef, private renderer: Renderer2, private rouletteService: RouletteService) {}

  ngOnInit() {
    this.renderer.addClass(this.el.nativeElement, this.colorClass);
  }
}
