import { Component } from '@angular/core';
import { RouletteService } from './shared/roulette.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  numbers: number[] = [];
  balance: number = 100;
  radioValue = 'red';
  betValue = 1;

  constructor(private rouletteService:  RouletteService) {}

  startGame() {
    this.rouletteService.start();
    this.rouletteService.newNumber.subscribe((number: number) => {
      this.numbers.push(number);
      if ((this.radioValue === 'zero') && this.rouletteService.getColor(number) === 'zero') {
        this.balance += (this.betValue * 35);
      } else if (this.radioValue === this.rouletteService.getColor(number)) {
        this.balance += (this.betValue * 1);
      } else {
        this.balance -= (this.betValue * 1);
      }
    });
  }

  stopGame() {
    this.rouletteService.stop();
  }

  resetGame() {
    this.numbers.splice(0);
  }
}
